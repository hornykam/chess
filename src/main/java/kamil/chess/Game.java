package kamil.chess;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import kamil.chess.types.Types.Color;
import kamil.chess.types.Types.TypeOfFigure;
import kamil.chess.ui.ChessUI;
import java.io.*;

/*
 * Hra sachy.
 */
/**
 *
 * @author Kamil Horny
 */
public class Game {

    private static final Logger LOG = Logger.getLogger(Game.class.getName());
    private Player whitePlayer;
    private Player blackPlayer;
    private final Rules rules;
    private Field fields[][];
    private ChessUI chessUI;
    private static Game game;
//    private boolean fieldIsChoonsen;
    private int roundNumber;
    private FileWriter fw;
    private FileReader rd;
    private BufferedWriter bw;
//    private boolean whitePlays = true;
//    private boolean blackPlays = false;
    private Timer t;
    private OnTurn onTurn;
    private String pgnFileName;

    //castling conditions
    private boolean blackLeftRookCastling = true;
    private boolean blackRightRookCastling = true;
    private boolean blackKingCastling = true;
    private boolean whiteLeftRookCastling = true;
    private boolean whiteRightRookCastling = true;
    private boolean whiteKingCastling = true;

    public Game() {//Player player1, Player player2) { //Chessboard chessboard, 
//if (true)throw new IllegalStateException("blee");

        this.chessUI = new ChessUI();
        this.t = new Timer("cas");
        chessUI.setGame(this);
        createChessboard();
        this.rules = new Rules();
        rules.setGame(this);
        chessUI.setVisible(true);
        t.start();
        newGame(new Player(Color.WHITE, "Kamil"), new Player(Color.BLACK, "Pepa"));
    }

    /** Starting a new Game.
     * 
     * @param p1 White player.
     * @param p2 Black player.
     */
    public void newGame(Player p1, Player p2) {
        Logger.getLogger(Game.class.toString()).log(Level.INFO, "Starting new Game");
        this.whitePlayer = p1;
        this.blackPlayer = p2;
        createPgnFile("file", p1, p2);
        this.whitePlayer.setTimeLeft(60 * 20 * 1000);
        this.blackPlayer.setTimeLeft(20 * 60 * 1000);
//        this.chessUI = new ChessUI();
//        this.t = new Timer("cas");
//        chessUI.setGame(this);
//        createChessboard();
        setAllFigures();
//        this.rules = new Rules();
        this.chessUI.paintAll(fields, whitePlayer, blackPlayer);
//        chessUI.setVisible(true);
        this.onTurn = OnTurn.WHITE;
//        t.start();
        this.roundNumber = 1;

    }

    private Figure removeFigure(int column, int row) {
        return fields[column][row].removeFigure();
    }

    private void setFigure(Figure fig, int column, int row) {
        fields[column][row].setFigure(fig);
    }

    public Figure getFigure(int column, int row) {
        return fields[column][row].getFigure();
    }

    public void setAllFigures() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                this.fields[i][j].setFigure(null);
            }
        }
        setFigure(createFigure(Color.WHITE, TypeOfFigure.PAWN), 0, 1);
        setFigure(createFigure(Color.WHITE, TypeOfFigure.PAWN), 1, 1);
        setFigure(createFigure(Color.WHITE, TypeOfFigure.PAWN), 2, 1);
        setFigure(createFigure(Color.WHITE, TypeOfFigure.PAWN), 3, 1);
        setFigure(createFigure(Color.WHITE, TypeOfFigure.PAWN), 4, 1);
        setFigure(createFigure(Color.WHITE, TypeOfFigure.PAWN), 5, 1);
        setFigure(createFigure(Color.WHITE, TypeOfFigure.PAWN), 6, 1);
        setFigure(createFigure(Color.WHITE, TypeOfFigure.PAWN), 7, 1);
        setFigure(createFigure(Color.WHITE, TypeOfFigure.ROOK), 0, 0);
        setFigure(createFigure(Color.WHITE, TypeOfFigure.KNIGHT), 1, 0);
        setFigure(createFigure(Color.WHITE, TypeOfFigure.BISHOP), 2, 0);
        setFigure(createFigure(Color.WHITE, TypeOfFigure.QUEEN), 3, 0);
        setFigure(createFigure(Color.WHITE, TypeOfFigure.KING), 4, 0);
        setFigure(createFigure(Color.WHITE, TypeOfFigure.BISHOP), 5, 0);
        setFigure(createFigure(Color.WHITE, TypeOfFigure.KNIGHT), 6, 0);
        setFigure(createFigure(Color.WHITE, TypeOfFigure.ROOK), 7, 0);
        //
        setFigure(createFigure(Color.BLACK, TypeOfFigure.PAWN), 0, 6);
        setFigure(createFigure(Color.BLACK, TypeOfFigure.PAWN), 1, 6);
        setFigure(createFigure(Color.BLACK, TypeOfFigure.PAWN), 2, 6);
        setFigure(createFigure(Color.BLACK, TypeOfFigure.PAWN), 3, 6);
        setFigure(createFigure(Color.BLACK, TypeOfFigure.PAWN), 4, 6);
        setFigure(createFigure(Color.BLACK, TypeOfFigure.PAWN), 5, 6);
        setFigure(createFigure(Color.BLACK, TypeOfFigure.PAWN), 6, 6);
        setFigure(createFigure(Color.BLACK, TypeOfFigure.PAWN), 7, 6);
        setFigure(createFigure(Color.BLACK, TypeOfFigure.ROOK), 0, 7);
        setFigure(createFigure(Color.BLACK, TypeOfFigure.ROOK), 7, 7);
        setFigure(createFigure(Color.BLACK, TypeOfFigure.KNIGHT), 1, 7);
        setFigure(createFigure(Color.BLACK, TypeOfFigure.KNIGHT), 6, 7);
        setFigure(createFigure(Color.BLACK, TypeOfFigure.BISHOP), 2, 7);
        setFigure(createFigure(Color.BLACK, TypeOfFigure.BISHOP), 5, 7);
        setFigure(createFigure(Color.BLACK, TypeOfFigure.KING), 4, 7);
        setFigure(createFigure(Color.BLACK, TypeOfFigure.QUEEN), 3, 7);
    }

    private Figure createFigure(Color col, TypeOfFigure typ) {
        return new Figure(col, typ);
    }

    public boolean blackLeftRookCastling() {
        return blackLeftRookCastling;
    }

    public boolean blackRightRookCastling() {
        return blackRightRookCastling;
    }

    public boolean blackKingCastling() {
        return blackKingCastling;
    }

    public boolean whiteLeftRookCastling() {
        return whiteLeftRookCastling;
    }

    public boolean whiteRightRookCastling() {
        return whiteRightRookCastling;
    }

    public boolean whiteKingCastling() {
        return whiteKingCastling;
    }

    private void resetCastling() {
        this.whiteKingCastling = false;
        this.whiteLeftRookCastling = false;
        this.whiteRightRookCastling = false;
        this.blackKingCastling = false;
        this.blackRightRookCastling = false;
        this.blackLeftRookCastling = false;
    }

    private void createChessboard() {
        this.fields = new Field[8][8];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if ((i + j) % 2 == 0) {
                    this.fields[i][j] = new Field(Color.BLACK, null);
                } else {
                    this.fields[i][j] = new Field(Color.WHITE, null);
                }
            }
        }

    }

    /** Main method.
     * 
     * @param args .
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ChessUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ChessUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ChessUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ChessUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    LOG.log(Level.INFO, "Starting a Game");
                    game = new Game();
                } catch (Exception ex) {
                    LOG.log(Level.SEVERE, "Error on Game", ex);
                }
//                game.clickOnField();
                //;,WHITE_PLAYER)
                //new ChessUI().setVisible(true);
            }
        });
    }

    private void createPgnFile(String pgnFileName, Player p1, Player p2) {
        try {
            this.pgnFileName = pgnFileName;
            this.fw = new FileWriter(pgnFileName + ".pgn");
            this.bw = new BufferedWriter(fw);
            //
            //date
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            bw.write("[" + dateFormat.format(date) + "]");
            bw.newLine();
            if (p1.getPlayersColor() == Color.WHITE) {
                bw.write("[White " + "\"" + p1.getPlaersColor() + "\"]");
                bw.write("[Black " + "\"" + p2.getPlaersColor() + "\"]");
            } else {
                bw.write("[White " + "\"" + p2.getPlaersColor() + "\"]");
                bw.write("[Black " + "\"" + p1.getPlaersColor() + "\"]");
            }
            bw.newLine();
            bw.close();
            fw.close();

        } catch (IOException ex) {
            System.err.println("Pri vytvareni se neco porouchalo!");
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean firstFieldCanBeChooosen(int x, int y) {
        if (onTurn == OnTurn.PLACING) {
            return true;
        }
        Color col;
        if (onTurn == OnTurn.BLACK) {
            col = Color.BLACK;
        } else {
            col = Color.WHITE;
        }
        return this.rules.canChooseFigureToMove(this.fields, x, y, col);
    }

    public boolean secondFieldCanBeChoosen(int x1, int y1, int x2, int y2) {
        if (onTurn == OnTurn.PLACING) {
            return true;
        }
        Color col;
        if (onTurn == OnTurn.BLACK) {
            col = Color.BLACK;
        } else {
            col = Color.WHITE;
        }
        return this.rules.figureCanMoveBetween(this.fields, x1, y1, x2, y2, col) && (!this.rules.sasch(this.fields, x1, y1, x2, y2));
    }

    public void makeMove(int x1, int y1, int x2, int y2) {
//        boolean rightSideCastling, leftSideCastling;
        Figure tmpFig = fields[x1][y1].removeFigure();
        fields[x2][y2].setFigure(tmpFig);
        chessUI.paintTwoFields(x1, y1, fields[x1][y1], x2, y2, fields[x2][y2]);
        //castling
        if (x1 == 7 && y1 == 0) {
            this.whiteLeftRookCastling = false;
        } else if (x1 == 4 && y1 == 0) {
            this.whiteRightRookCastling = false;
        } else if (x1 == 0 && y1 == 0) {
            this.whiteKingCastling = false;
        } else if (x1 == 7 && y1 == 7) {
            this.blackLeftRookCastling = false;
        } else if (x1 == 4 && y1 == 7) {
            this.blackRightRookCastling = false;
        } else if (x1 == 0 && y1 == 7) {
            this.blackKingCastling = false;
        }

        int c = makeCastling(x1, y1, x2, y2);
        boolean sasch = this.rules.sasch(fields, tmpFig.getColor());//mozna obracene
        boolean mate = this.rules.mate(fields, tmpFig.getColor());

        writeMove(fields[x2][y2].getFigure(), x2, y2, sasch, mate, c == 1, c == 2);

        //changing player
        if (this.onTurn == OnTurn.WHITE) {
            this.onTurn = OnTurn.BLACK;
        } else if (this.onTurn == OnTurn.BLACK) {
            this.onTurn = OnTurn.WHITE;

            this.roundNumber++;
        }
    }

    public int makeCastling(int x1, int y1, int x2, int y2) {
        if (x1 == 4 && y1 == 0 && x2 == 6 && y2 == 0) {
            Figure tmpFig = fields[7][0].removeFigure();
            fields[5][0].setFigure(tmpFig);
            chessUI.paintTwoFields(7, 0, fields[7][0], 5, 0, fields[5][0]);
            this.whiteRightRookCastling = false;
            this.whiteKingCastling = false;
            return 1;//prava
        } else if (x1 == 4 && y1 == 0 && x2 == 2 && y2 == 0) {
            Figure tmpFig = fields[0][0].removeFigure();
            fields[3][0].setFigure(tmpFig);
            chessUI.paintTwoFields(0, 0, fields[0][0], 3, 0, fields[3][0]);
            this.whiteLeftRookCastling = false;
            this.whiteKingCastling = false;
            return 2; //leva
        } else if (x1 == 4 && y1 == 7 && x2 == 6 && y2 == 7) {
            Figure tmpFig = fields[7][7].removeFigure();
            fields[5][7].setFigure(tmpFig);
            chessUI.paintTwoFields(7, 7, fields[7][7], 5, 7, fields[5][7]);
            this.blackRightRookCastling = false;
            this.blackKingCastling = false;
            return 1;//prava
        } else if (x1 == 4 && y1 == 7 && x2 == 1 && y2 == 7) {
            Figure tmpFig = fields[0][7].removeFigure();
            fields[3][7].setFigure(tmpFig);
            chessUI.paintTwoFields(0, 7, fields[0][7], 3, 7, fields[3][7]);
            this.blackLeftRookCastling = false;
            this.blackKingCastling = false;
            return 2; //leva
        } else {
            return 0;
        }
    }

    private void writeMove(Figure fig1, int x1, int y1, boolean check, boolean checkmate, boolean rightSideCastling, boolean leftSideCastling) {
        try {
            this.fw = new FileWriter(this.pgnFileName, true);
            this.bw = new BufferedWriter(fw);
            String str = "";
            if (fig1.getColor() == Color.WHITE) {
                str = roundNumber + ". ";
            }
            if (rightSideCastling) {
                str += "O-O";
            } else if (leftSideCastling) {
                str += "O-O-O";
            } else {
                switch (fig1.getType()) {
                    case BISHOP:
                        str += "B";
                        break;
                    case KING:
                        str += "K";
                        break;
                    case KNIGHT:
                        str += "N";
                        break;
                    case QUEEN:
                        str += "Q";
                        break;
                    case ROOK:
                        str += "R";
                        break;
                    case PAWN:
                        break;
                }
                switch (x1) {
                    case 0:
                        str += "a";
                        break;
                    case 1:
                        str += "b";
                        break;
                    case 2:
                        str += "c";
                        break;
                    case 3:
                        str += "d";
                        break;
                    case 4:
                        str += "e";
                        break;
                    case 5:
                        str += "f";
                        break;
                    case 6:
                        str += "g";
                        break;
                    case 7:
                        str += "h";
                        break;
                }
            }
            str += y1 + 1;
            if (check) {
                str += "+";
            } else if (checkmate) {
                str += "#";
            }
            str += " ";
            bw.write(str);
            if (fig1.getColor() == Color.BLACK) {
                bw.newLine();
            }
            bw.close();
            fw.close();
            chessUI.addMessage(str);

        } catch (IOException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void setManual(boolean b) {
        if (b) {
            newGame(new Player(Color.WHITE, "Kamil"), new Player(Color.BLACK, "Pepa"));
            this.onTurn = OnTurn.PLACING;
        } else {
            this.onTurn = OnTurn.WHITE;
        }
    }

//    private void changePlayerOnTurn() {
//
//    }
    public enum State {
        NORMAL, SASCH_WHITE, SASCH_BLACK, WIN_WHITE, WIN_BLACK
    }

    public enum OnTurn {
        WHITE, BLACK, NONE, PLACING
    }

    public OnTurn getOnTurn() {
        return onTurn;
    }

    private void checkTime(long diference) {
        Player p;
        if (this.onTurn == OnTurn.WHITE) {
            p = this.whitePlayer;
        } else {
            p = this.blackPlayer;
        }
        long plMiliLeft = p.getTimeLeft();
        plMiliLeft -= diference;
        p.setTimeLeft(plMiliLeft);
        this.chessUI.setPlayersTimeLeft(p);
    }

    class Timer implements Runnable {

        private Thread t;
        private String threadName;

        Timer(String name) {
            threadName = name;
            System.out.println("Creating " + threadName);
        }

        public void run() {
            System.out.println("Running " + threadName);
            try {
                do {
                    Date first = new Date();
                    Thread.sleep(1000);
                    Date second = new Date();
                    checkTime(second.getTime() - first.getTime());
                } while (true);
            } catch (InterruptedException e) {
                LOG.log(Level.SEVERE, "ERROR in: " + threadName, e);
            }
        }

        public void start() {
            System.out.println("Starting " + threadName);
            if (t == null) {
                t = new Thread(this, threadName);
                t.start();
            }
        }
    }

}

package kamil.chess.types;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Contains all possible colors and types of fields and figures.
 *
 * @author Uzivatel
 */
public class Types {

    /**
     * Two colors that are in game
     */
    public static enum Color {

        /**
         * White
         */
        WHITE,
        /**
         * Black
         */
        BLACK
    }

    /**
     * All types of figure.
     */
    public enum TypeOfFigure {

        /**
         * King
         */
        KING,
        /**
         * Queen
         */
        QUEEN,
        /**
         * Rook
         */
        ROOK,
        /**
         * Knight
         */
        KNIGHT,
        /**
         * Bishop
         */
        BISHOP,
        /**
         * Pawn
         */
        PAWN

    }
}

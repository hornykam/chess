package kamil.chess;

import kamil.chess.types.Types.TypeOfFigure;
import kamil.chess.types.Types.Color;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Represents one field of chessboard.
 *
 * @author Uzivatel
 */
public class Field implements Cloneable{

    private final Color color;
    private Figure figure;

    /**
     * Represents one field of chessboard.
     *
     * @param color Color of field.
     * @param figure Figure that is on field.
     */
    public Field(Color color, Figure figure) {
        this.color = color;
        this.figure = figure;
    }

    /**
     *
     * @return Return color of field.
     */
    public Color getColor() {
        return this.color;
    }

    /**
     *
     * @return Return figure that stands on field.
     */
    public Figure getFigure() {
        return this.figure;
    }

    /**
     * Set figure on field.
     * @param f Figure
     */
    public void setFigure(Figure f) {
        this.figure = f;
    }

    /**
     *
     * @return Return Figure that is removed from field.
     */
    public Figure removeFigure() {
        Figure fig = this.figure;
        this.figure = null;
        return fig;
    }

    @Override
    protected Field clone() {
        return new Field(color, figure); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}

package kamil.chess;

import kamil.chess.types.Types.Color;
import kamil.chess.types.Types.TypeOfFigure;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Rules, class that contains all possible rules.
 *
 * @author Uzivatel
 */
public class Rules {

    private Game game;

    /**
     *
     * @param p
     * @return
     */
    public boolean playerCantMove(Player p) {
        return false;
    }

    /**
     * Check if figure can move between from first field to the second field.
     *
     * @param x1 First coordinate of first field
     * @param y1 Second coordinate of the first field
     * @param x2 First coordinate of second field
     * @param y2 First coordinate of second field
     * @param col Color of the figure that is supposed to move.
     * @return Returns true, if the required move is according to the rules.
     */
    public boolean figureCanMoveBetween(Field fields[][], int x1, int y1, int x2, int y2, Color col) {
        if (col == getFigureColor(fields, x1, y1)) {
            if (!checkStartAndFinalFields(fields, x1, y1, x2, y2)) {
                return false;
            }
            TypeOfFigure t = getFigure(fields, x1, y1).getType();
            switch (t) {
                case BISHOP:
                    return bishop(fields, x1, y1, x2, y2);
                case KING:
                    return king(fields, x1, y1, x2, y2);
                case KNIGHT:
                    return knight(fields, x1, y1, x2, y2);
                case PAWN:
                    return pawn(fields, x1, y1, x2, y2);
                case QUEEN:
                    return queen(fields, x1, y1, x2, y2);
                case ROOK:
                    return rook(fields, x1, y1, x2, y2);
            }
        }
        return false;
    }

    /**
     * Check if player can move with figure.
     *
     * @param x First coordinate of the first choosen field.
     * @param y Second coordinate of the second choosen field.
     * @param col Color of player that is on turn.
     * @return Returns true, if player can choose figure.
     */
    public boolean canChooseFigureToMove(Field[][] fields, int x, int y, Color col) {
        if (fields[x][y].getFigure() == null || fields[x][y].getFigure().getColor() != col) {
            return false;
        }
        return true;

    }

    /**
     * Pawn promoting
     *
     * @param x First coordinate of pawn.
     * @param y Second coordinate of pawn.
     * @return Returns ture, if pawn can promote.
     */
    public boolean pawnPromote(Field[][] fields, int x, int y) {
        if (getFigure(fields, x, y).getType() == TypeOfFigure.PAWN) {
            return false;
        }
        if (y == 1) {
            if (getFigureColor(fields, x, y) == Color.BLACK) {
                return true;
            }
        } else if (y == 8) {
            if (getFigureColor(fields, x, y) == Color.WHITE) {
                return true;
            }
        }
        return false;

    }

    //šach
    private boolean king(Field fields[][], int x1, int y1, int x2, int y2) {
        if (getFigureColor(fields, x1, y1) == Color.WHITE) {
            if (this.game.whiteKingCastling() && this.game.whiteRightRookCastling() && x1 == 4 && y1 == 0 && x2 == 6 && y2 == 0) {
                return true;
            } else if (this.game.whiteLeftRookCastling() && this.game.whiteKingCastling() && x1 == 4 && y1 == 0 && x2 == 2 && y2 == 0) {
                return true;
            }
        } else if (getFigureColor(fields, x1, y1) == Color.BLACK) {
            if (this.game.blackRightRookCastling() && this.game.blackKingCastling() && x1 == 4 && y1 == 7 && x2 == 6 && y2 == 7) {
                return true;
            } else if (this.game.blackLeftRookCastling() && this.game.blackKingCastling() && x1 == 4 && y1 == 7 && x2 == 2 && y2 == 7) {
                return true;
            }
        }

        return ((x1 == x2 || x1 == x2 + 1 || x1 == x2 - 1) && (y1 == y2 || y1 == y2 + 1 || y1 == y2 - 1));
    }

    private boolean knight(Field fields[][], int x1, int y1, int x2, int y2) {
        return (x1 == (x2 + 1) && (y1 == y2 + 2))
                || (x1 == (x2 + 1) && (y1 == y2 - 2))
                || (x1 == (x2 - 1) && (y1 == y2 - 2))
                || (x1 == (x2 - 1) && (y1 == y2 + 2))
                || (x1 == (x2 + 2) && (y1 == y2 + 1))
                || (x1 == (x2 + 2) && (y1 == y2 - 1))
                || (x1 == (x2 - 2) && (y1 == y2 - 1))
                || (x1 == (x2 - 2) && (y1 == y2 + 1));
    }

    // zatim chybi En Passant
    private boolean pawn(Field fields[][], int x1, int y1, int x2, int y2) {
        if (null == getFigureColor(fields, x1, y1)) {
            System.out.println("nesmi se stat");
            return false;
        } else if (x1 == x2 && getFigure(fields, x2, y2) != null) {
            return false;
        } else {
            switch (getFigureColor(fields, x1, y1)) {
                case WHITE:
//                    System.out.println("x1 " + x1 + " y1 " + y1 + " x2 " + x2 + " y2 " + y2);
                    return (x1 == x2 && y1 + 1 == y2) || (x1 == x2 && y1 == 1 && y2 == 3 && getFigure(fields, x1, 2) == null)
                            || (x2 == (x1 + 1) && (y2 == y1 + 1) && (getFigure(fields, x2, y2) != null) && (getFigureColor(fields, x2, y2) == Color.BLACK))
                            || (x2 == (x1 - 1) && (y2 == y1 + 1) && (getFigure(fields, x2, y2) != null) && (getFigureColor(fields, x2, y2) == Color.BLACK));//chybi brani mimochodem
                case BLACK:
                    return (x1 == x2 && y1 - 1 == y2) || (x1 == x2 && y1 == 6 && y2 == 4 && getFigure(fields, x1, 5) == null)
                            || (x2 == (x1 + 1) && (y2 == y1 - 1) && (getFigure(fields, x2, y2) != null) && (getFigureColor(fields, x2, y2) == Color.WHITE))
                            || (x2 == (x1 - 1) && (y2 == y1 - 1) && (getFigure(fields, x2, y2) != null) && (getFigureColor(fields, x2, y2) == Color.WHITE));//chybi brani mimochodem
                default:
                    return false;
            }
        }
    }

    private boolean queen(Field fields[][], int x1, int y1, int x2, int y2) {

        return rook(fields, x1, y1, x2, y2) || bishop(fields, x1, y1, x2, y2);

    }

    private boolean checkStartAndFinalFields(Field fields[][], int x1, int y1, int x2, int y2) {
        if (getFigure(fields, x2, y2) == null) {
            return true;
        }
        return (getFigureColor(fields, x1, y1) != getFigureColor(fields, x2, y2)) && !(x1 == x2 && y1 == y2) && (getFigure(fields, x1, y1) != null);
    }

    private boolean checkFreeField(Field fields[][], int x, int y) {
        return getFigure(fields, x, y) == null;
    }

    private boolean rook(Field fields[][], int x1, int y1, int x2, int y2) {
        if (x1 == x2) {
            if (y2 > y1) {
                for (int i = y1 + 1; i < y2; i++) {
                    if (getFigure(fields, x1, i) != null) {
                        return false;
                    }
                }
            } else if (y1 > y2) {
                for (int i = y1 - 1; i > y2; i--) {
                    if (getFigure(fields, x1, i) != null) {
                        return false;
                    }
                }
            }
        } else if (y1 == y2) {
            if (x2 > x1) {
                for (int i = x1 + 1; i < x2; i++) {
                    if (getFigure(fields, i, y1) != null) {
                        return false;
                    }
                }
            } else if (x1 > x2) {
                for (int i = x1 - 1; i > x2; i--) {
                    if (getFigure(fields, i, y1) != null) {
                        return false;
                    }
                }
            }
        } else if (x1 != x2 && y1 != y2) {
            return false;
        }
        return true;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    private boolean bishop(Field fields[][], int x1, int y1, int x2, int y2) {

        int dir;
        if (x1 < x2 && y1 < y2) {
            dir = 0;
        } else if (x1 > x2 && y1 < y2) {
            dir = 1;
        } else if (x1 > x2 && y1 > y2) {
            dir = 2;
        } else if (x1 < x2 && y1 > y2) {
            dir = 3;
        } else {
            dir = 5;
        }
        int i = y2 - y1;
        int jj = x1 - x2;

        switch (dir) {
            case 0:
                if (x2 - x1 != y2 - y1) {
                    return false;
                }
                for (int j = 1; j < i; j++) {
                    if (!checkFreeField(fields, x1 + j, y1 + j)) {
                        return false;
                    }
                }
                return true;

            case 1:
                if (x1 - x2 != y2 - y1) {
                    return false;
                }
                for (int j = 1; j < i; j++) {
                    if (!checkFreeField(fields, x1 - j, y1 + j)) {
                        return false;
                    }
                }
                return true;

            case 2:
                if (x1 - x2 != y1 - y2) {
                    return false;
                }
                for (int j = 1; j < jj; j++) {
                    if (!checkFreeField(fields, x1 - j, y1 - j)) {
                        return false;
                    }
                }
                return true;
            case 3:
                if (x2 - x1 != y1 - y2) {
                    return false;
                }
                for (int j = 1; j < jj; j++) {
                    if (!checkFreeField(fields, x1 + j, y1 - j)) {
                        return false;
                    }
                }
                return true;
            default:
                return false;
        }
    }

    private boolean palyerCanMoveOnField(Field fields[][], int x, int y, Color c) {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (c == getFigureColor(fields, i, j) && figureCanMoveBetween(fields, i, j, x, y, c)) {
                    return true;
                }
            }
        }
        return false;
    }

    private Figure getFigure(Field fields[][], int x, int y) {
        return fields[x][y].getFigure();
    }

    private Color getFigureColor(Field fields[][], int x, int y) {
        if (fields[x][y].getFigure() == null) {
            return null;
        }
        return fields[x][y].getFigure().getColor();
    }

    /**
     * Set variable fields.
     *
     * @param f Variable on wich is changed fields.
     */
//    public void setFields(Field[][] f) {
//        this.fields = f;
//    }
    /**
     *
     * @param x
     * @param y
     * @param fig
     */
//    public void setCastling(int x, int y, Figure fig) {
//        if ((fig.getColor() == Color.WHITE) && x == 0 && y == 0) {
//            whiteLeftRookCastling = false;
//        } else if ((fig.getColor() == Color.WHITE) && x == 7 && y == 0) {
//            whiteRightRookCastling = false;
//        } else if ((fig.getColor() == Color.WHITE) && x == 4 && y == 0) {
//            whiteKingCastling = false;
//            whiteRightRookCastling = false;
//            whiteLeftRookCastling = false;
//        }
//
//        if (!whiteLeftRookCastling && !whiteRightRookCastling) {
//            whiteKingCastling = false;
//        }
//
//        if ((fig.getColor() == Color.BLACK) && x == 0 && y == 0) {
//            blackLeftRookCastling = false;
//        } else if ((fig.getColor() == Color.BLACK) && x == 7 && y == 0) {
//            blackRightRookCastling = false;
//        } else if ((fig.getColor() == Color.BLACK) && x == 4 && y == 0) {
//            blackKingCastling = false;
//            blackRightRookCastling = false;
//            blackLeftRookCastling = false;
//        }
//        if (!blackLeftRookCastling && !blackRightRookCastling) {
//            blackKingCastling = false;
//        }
//    }
    /**
     *
     * @param f
     * @return
     */
//    public boolean canCastlingHappen(Figure f) {
//        return (f.getType() == TypeOfFigure.KING) && (whiteKingCastling || blackKingCastling);
//    }
    /**
     * Cecck if move between two fields leads doesn't cause him sasch.
     *
     * @param x1 First coordinate of first field.
     * @param y1 Second coordinate of first field.
     * @param x2 First coordinate of first field.
     * @param y2 Second coordinate of second field.
     * @return Return true if players own king is in safe.
     */
    public boolean sasch(Field[][] fields, int x1, int y1, int x2, int y2) {

        Field[][] tempFields = new Field[8][8];
        Color kc = getFigure(fields, x1, y1).getColor();
     
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                tempFields[i][j] = fields[i][j].clone();
            }
        }
        Figure f = tempFields[x1][y1].removeFigure();
        tempFields[x2][y2].setFigure(f);
        return sasch(tempFields, kc);
    }

    public boolean sasch(Field[][] fields, Color col) {
       Color ko = (col == Color.WHITE) ? Color.BLACK : Color.WHITE;
       int kx = -1, ky = -1;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (fields[i][j].getFigure() != null && fields[i][j].getFigure().getType() == TypeOfFigure.KING) {
                    if (fields[i][j].getFigure().getColor() == col) {
//                    if (getFigureColor(fields,i, j) == kc) {
                        kx = i;
                        ky = j;
                        break;
                    }
                }
            }
        }

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (fields[i][j].getFigure() != null && figureCanMoveBetween(fields, i, j, kx, ky, ko)) {
                    System.out.println("i: " + i + " j: " + j);
                    System.out.println("byl by sach");
                    return true;
                }
            }
        }
        return false;
    }
    
    public boolean mate(Field[][] fields, Color col) {
//       Color ko = (col == Color.WHITE) ? Color.BLACK : Color.WHITE;
//       int kx = -1, ky = -1;
//        for (int i = 0; i < 8; i++) {
//            for (int j = 0; j < 8; j++) {
//                if (fields[i][j].getFigure() != null && fields[i][j].getFigure().getType() == TypeOfFigure.KING) {
//                    if (fields[i][j].getFigure().getColor() == col) {
////                    if (getFigureColor(fields,i, j) == kc) {
//                        kx = i;
//                        ky = j;
//                        break;
//                    }
//                }
//            }
//        }
//        for (int i = 0; i < 8; i++) {
//            for (int j = 0; j < 8; j++) {
//                if (fields[i][j].getFigure() != null && figureCanMoveBetween(fields, i, j, kx, ky, ko)) {
//                    System.out.println("i: " + i + " j: " + j);
//                    System.out.println("byl by sach");
//                    return true;
//                }
//            }
//        }
        return false;
    }

}

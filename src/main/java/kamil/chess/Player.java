package kamil.chess;

import java.util.Date;
import kamil.chess.types.Types.Color;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.

/**
 * Represent player, that playes chess.
 * @author Uzivatel
 */


public class Player {
    private final Color COLOUR;
    private final String NAME;
    private long milisecondsLeft;

    /**
     * 
     * @param color Color, which player plays.
     * @param name Name of Player.
     */
    public Player(Color color, String name){
        this.COLOUR = color;
        this.NAME = name;
    }
    
    /**
     * 
     * @return Returns players name as String.
     */
    public String getPlayersName(){
        return this.NAME;
    }
    
    /**
     *
     * @return Return players color as Color.
     */
    public Color getPlayersColor(){
        return this.COLOUR;
    }
    
    /**
     *
     * @return Returns players color as String.
     */
    public String getPlaersColor(){
        return this.COLOUR.toString();
    }

    public long getTimeLeft() {
        return this.milisecondsLeft;
    }

    public void setTimeLeft(long milisLeft) {
        this.milisecondsLeft = milisLeft;
    }
    
    
    
}

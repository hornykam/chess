/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kamil.chess;

import kamil.chess.types.Types;
import java.util.ArrayList;

/**
 *
 * @author Uzivatel
 */
public class Computer extends Player {

    private Game game;
    private Timer t2;

    public Computer(Game game) {
        super(Types.Color.BLACK, "Kasparov");
        this.game = game;
        this.t2 = new Timer("cas");
        t2.start();
    }

    private void move() {
        ArrayList<Fxy> figures = findAllFigures();
        Fxy randomFigure;// = chooseFxy(figures);
        ArrayList<Fxy> fields;// = findAllFields();
        Fxy randomField ;//= chooseFxy(fields);
        do {
            randomFigure = chooseFxy(figures);
            fields=findAllFields();
            do {
                randomField = chooseFxy(fields);
                if (this.game.secondFieldCanBeChoosen(randomFigure.x, randomFigure.y, randomField.x, randomField.y)) {
                    this.game.makeMove(randomFigure.x, randomFigure.y, randomField.x, randomField.y);
                    break;
                }
                fields.remove(randomField);
            } while (fields.size() > 0);

            figures.remove(randomFigure);
        } while (figures.size() > 0);
    }

    private ArrayList<Fxy> findAllFigures() {
        ArrayList<Fxy> blackFigures = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (this.game.getFigure(i, j) != null) {
                    if (this.game.getFigure(i, j).getColor() == Types.Color.BLACK) {
                        blackFigures.add(new Fxy(i, j));
                    }
                }
            }
        }
        return blackFigures;
    }

    private Fxy chooseFxy(ArrayList<Fxy> blackFigures) {
        if(blackFigures.size()==1){
            return blackFigures.get(0);
        }
        return blackFigures.get((int) (Math.random() * (blackFigures.size()-1)));
    }

    private ArrayList<Fxy> findAllFields() {
        ArrayList<Fxy> allFields = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                allFields.add(new Fxy(i, j));
            }
        }
        return allFields;
    }

    private class Fxy {

        int x, y;

        public Fxy(int x, int y) {
            this.x = x;
            this.y = y;
        }

    }

    private void tick() {
        if (this.game.getOnTurn() == Game.OnTurn.BLACK) {
            move();
        }
    }

    class Timer implements Runnable {

        private Thread t;
        private String threadName;

        Timer(String computerTimer) {
            threadName = computerTimer;
        }

        public void run() {
            try {
                do {
                    Thread.sleep(2000);
                    tick();
                } while (true);
            } catch (InterruptedException e) {
                System.out.println("Thread " + threadName + " interrupted.");
            }
        }

        public void start() {
            if (t == null) {
                t = new Thread(this, threadName);
                t.start();
            }
        }
    }

}

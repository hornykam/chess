package kamil.chess;


import kamil.chess.types.Types.Color;
import kamil.chess.types.Types.TypeOfFigure;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Represents figures. 
 * @author Uzivatel
 */
public class Figure {

    private final Color COLOUR;
    private final TypeOfFigure TYPE;

    /**
     *
     * @param colour Coloer of figure
     * @param type Type of figure
     */
    public Figure(Color colour, TypeOfFigure type) {
        this.COLOUR = colour;
        this.TYPE = type;
    }

    /**
     *
     * @return Return coloer of Figure
     */
    public Color getColor(){
        return this.COLOUR;
    }

    
    
    /**
     *
     * @return Type of figure.
     */
    public TypeOfFigure getType(){
        return this.TYPE;
    }

    /**
     *
     * @return Returns type of figure in String.
     */
    public String getTypeInString() {
        return "" + this.TYPE;
    }

    /**
     *
     * @return Returns color of figure in String.
     */
    public String getColourString() {
        return "" + this.COLOUR;
    }
}
